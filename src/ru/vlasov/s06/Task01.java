package ru.vlasov.s06;

public class Task01 {
    public static void main(String[] args) {
        Circle circle1 = new Circle(0, 0, 12);
        Circle circle2 = new Circle(3, 5, -14);
        if (circle1.getRadius() > 0 & circle2.getRadius() > 0) {
            comparisonLengthCircle(circle1, circle2);
        } else {
            System.out.println("Радиус не может быть меньше 0");
        }
    }

    public static void comparisonLengthCircle(Circle circle1, Circle circle2) {
        if (circle1.getLengthCircle() == circle2.getLengthCircle()) {
            System.out.println("Длина двух окружностей равна");
        } else {
            System.out.println("Длина двух окружностей НЕ равна");
        }
    }
}