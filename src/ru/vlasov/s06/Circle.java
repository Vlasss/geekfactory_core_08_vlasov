package ru.vlasov.s06;

public class Circle {
    private int centerCircleX;
    private int centerCircleY;
    private int radius;
    private double lengthCircle;

    public Circle(int centerCircleX, int centerCircleY, int radius) {
        this.centerCircleX = centerCircleX;
        this.centerCircleY = centerCircleY;
        this.radius = radius;
        this.lengthCircle = 2 * Math.PI * radius;
    }

    public int getCenterCircleX() {
        return centerCircleX;
    }

    public void setCenterCircleX(int centerCircleX) {
        this.centerCircleX = centerCircleX;
    }

    public int getCenterCircleY() {
        return centerCircleY;
    }

    public void setCenterCircleY(int centerCircleY) {
        this.centerCircleY = centerCircleY;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public double getLengthCircle() {
        return lengthCircle;
    }

    public void setLengthCircle(double lengthCircle) {
        this.lengthCircle = lengthCircle;
    }
}