package ru.vlasov.s10;

import java.util.HashMap;

public class MyHashSet<T> implements Set<T>{
    private HashMap<T,Object> hashSet;
    private Object stopper = new Object();

    public MyHashSet() {
        hashSet = new HashMap<>();
    }

    @Override
    public boolean add(T key) {
        return hashSet.put(key, stopper)==null;
    }

    @Override
    public T remove(T key) {
            return (T) hashSet.remove(key);
    }

    @Override
    public boolean contains(T key) {
        return  hashSet.containsKey(key);
    }

    @Override
    public int size() {
        return hashSet.size();
    }
}