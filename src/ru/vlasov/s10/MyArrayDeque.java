package ru.vlasov.s10;

import java.util.Arrays;

public class MyArrayDeque<T> implements Queue<T> {
    int head = 0;
    private T[] elements;
    private int capacity = 20;

    public MyArrayDeque() {
        elements = (T[]) new Object[capacity];
    }

    @Override
    public boolean add(T e) {
        checkSize();
        if (e != null) {
            elements[head] = e;
            head++;
            return true;
        } else {
            return false;
        }
    }

    private void checkSize() {
        if (head == capacity) {
            capacity = capacity + capacity / 2;
            T[] buf = elements;
            elements = (T[]) new Object[capacity];
            System.arraycopy(buf, 0, elements, 0, head);
        }
    }

    @Override
    public T poll() {
        if (elements[0] != null) {
            T[] buf = elements;
            System.arraycopy(buf, 1, elements, 0, elements.length - 1);
            return buf[0];
        } else {
            return null;
        }
    }

    @Override
    public T peek() {
        if (elements[0] != null) {
            return elements[0];
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "MyArrayDeque{" +
                "head=" + head +
                ", elements=" + Arrays.toString(elements) +
                ", capacity=" + capacity +
                '}';
    }
}