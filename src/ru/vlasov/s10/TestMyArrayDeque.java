package ru.vlasov.s10;

import java.util.ArrayDeque;

public class TestMyArrayDeque {
    public static void recordingDifferentNumberElements() {
        ArrayDeque<Integer> testArrayDeque = new ArrayDeque<>();
        MyArrayDeque<Integer> testMyArrayDeque = new MyArrayDeque<>();
        int count = 1;
        for (int i = 0; i <= 5; i++) {
            count = count * 10;
            testDifferentNumberElements(count, testArrayDeque, testMyArrayDeque);
        }
    }

    private static void testDifferentNumberElements(int count, ArrayDeque testArrayDeque, MyArrayDeque testMyArrayDeque) {
        timeRecordsElementsToArrayDeque(count, testArrayDeque, testMyArrayDeque);
        timeGetDeleteElementsToArrayDeque(count, testArrayDeque, testMyArrayDeque);
    }

    private static void timeRecordsElementsToArrayDeque(int count, ArrayDeque testArrayDeque, MyArrayDeque testMyArrayDeque) {
        long timeStartRecordsArrayDeque = System.nanoTime();
        for (int i = 0; i < count; i++) {
            testArrayDeque.add(i);
        }
        timeStartRecordsArrayDeque = System.nanoTime() - timeStartRecordsArrayDeque;
        System.out.println("Время записи " + count + " элементов ArrayDeque =  " + timeStartRecordsArrayDeque + " ns");

        long timeStartRecordsMyArrayDeque = System.nanoTime();
        for (int i = 0; i < count; i++) {
            testMyArrayDeque.add(i);
        }
        timeStartRecordsMyArrayDeque = System.nanoTime() - timeStartRecordsMyArrayDeque;
        System.out.println("Время записи " + count + " элементов MyArrayDeque = " + timeStartRecordsMyArrayDeque + " ns \n");
    }

    private static void timeGetDeleteElementsToArrayDeque(int count, ArrayDeque testArrayDeque, MyArrayDeque testMyArrayDeque) {
        long timeStartGetArrayDeque = System.nanoTime();
        for (int i = 0; i < count; i++) {
            testArrayDeque.poll();
        }
        timeStartGetArrayDeque = System.nanoTime() - timeStartGetArrayDeque;
        System.out.println("Время получения и удаления " + count + " элементов ArrayDeque =  " + timeStartGetArrayDeque + " ns ");

        long timeStartGetMyArrayDeque = System.nanoTime();
        for (int i = 0; i < count; i++) {
            testMyArrayDeque.poll();
        }
        timeStartGetMyArrayDeque = System.nanoTime() - timeStartGetMyArrayDeque;
        System.out.println("Время получения и удаления " + count + " элементов MyArrayDeque = " + timeStartGetMyArrayDeque + " ns  \n");
    }
}