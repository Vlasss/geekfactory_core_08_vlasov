package ru.vlasov.s10;

public interface Set<T> {
    boolean add(T element);
    T remove(T element);
    boolean contains(T element);
    int size();
}