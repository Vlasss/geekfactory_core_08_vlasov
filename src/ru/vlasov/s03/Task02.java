package ru.vlasov.s03;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово");
        String word = sc.nextLine();
        System.out.println(addStarVersion1(word));
        System.out.println(addStarVersion2(word));
    }

    private static StringBuilder addStarVersion1(String word) {
        StringBuilder wordToStringBuilder = new StringBuilder(word);
        StringBuilder starToWord = new StringBuilder(word.length());
        for (int i = 0; i < word.length(); i++) {
            starToWord.append("**");
        }
        return starToWord.insert(wordToStringBuilder.length(),wordToStringBuilder);
    }

    private static String addStarVersion2(String word) {
        int wordLen = word.length();
        for (int i = 0; i < wordLen; i++) {
            word = "*" + word + "*";
        }
        return word;
    }
}