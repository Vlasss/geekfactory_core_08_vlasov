package ru.vlasov.s03;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите текст: ");
        String word = sc.nextLine();
        System.out.println("Выберите способ решения где 1 поиск слов по пробелу, а 2 метод split: ");
        int x = sc.nextInt();

        if (x == 1) {
            searchSpace(word);
        } else {
            searchSplit(word);
        }
    }

    public static void searchSpace(String word) {
        int counter = 1;
        char symbol = ' ';
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == symbol) {
                counter++;
            }
        }
        System.out.println("Слов в тексте: " + counter);
    }

    public static void searchSplit(String word) {
        String[] arrayword = word.split(" ");
        System.out.println("Слов в тексте: " + arrayword.length);
    }
}