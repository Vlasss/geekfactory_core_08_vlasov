package ru.vlasov.s03;

import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите текст: ");
        String word = sc.nextLine();
        System.out.println("Введите искомый символ: ");
        String s = sc.nextLine();
        char a = s.charAt(s.length()-1);
        searchSymbol(word, a);
    }

    public static void searchSymbol(String word, char symbol) {
        int counter = 0;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == symbol) {
                counter++;
            }
        }
        System.out.println(counter);
    }
}