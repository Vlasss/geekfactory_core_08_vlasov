package ru.vlasov.s03;

public class Task06 {

    public static void main(String[] args) {
        String word = "123456";
        char[] arrayword = word.toCharArray();
        int sum = Task06.sum(arrayword);
         System.out.println(sum);
    }

    public static int sum(char[] arrayword) {
        int rezult = 0;
        for (int i = arrayword.length - 1; i >= 0; i--) {
            int i1 = Character.getNumericValue(arrayword[i]);
            rezult = rezult + i1;
        }
        return rezult;
    }
}