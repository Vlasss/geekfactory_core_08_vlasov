package ru.vlasov.s03;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        char[] arrayWord = word.toCharArray();
        flipTheWord(arrayWord);
    }

    public static void flipTheWord(char arrayWord[]) {
        for (int i = arrayWord.length - 1; i >= 0; i--) {
            System.out.print(arrayWord[i]);
        }
    }
}