package ru.vlasov.s04;

public enum Alphabet {
    A, B, C, D, E, F, G, H, I, K, L, M, N, O, P, Q, R, S, T, V, X, Y, Z;

    public static int getLetterPosition(String letter) {
        letter = letter.toUpperCase();
        Alphabet possih = Alphabet.valueOf(letter);
        return possih.ordinal() + 1;
    }
}