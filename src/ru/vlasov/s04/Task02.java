package ru.vlasov.s04;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите букву английского алфавита: ");
        String letter = scanner.nextLine();
        System.out.println("Позиция буквы в алфавите: " + Alphabet.getLetterPosition(letter));
    }
}