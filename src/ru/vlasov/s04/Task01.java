package ru.vlasov.s04;

public class Task01 {
    public static void main(String[] args) {
        Hero hero1 = new Hero();
        hero1.setName("Добрыня Никитич, богатырь всея Руси");
        hero1.setSex("Мужской");
        hero1.setAge(24);
        System.out.println(hero1.getName());
        System.out.println(hero1.getSex());
        System.out.println(hero1.getAge());
        System.out.println(hero1.toString());

        Hero hero2 = new Hero("Алеша Попович", "Мужской", 25);
        Hero hero3 = new Hero("Илья Муромец", "Мужской");
        Hero enemy = new Hero("Змей Горыныч");
        System.out.println(hero2);
        System.out.println(hero3);
        System.out.println(enemy);
    }
}