package ru.vlasov.s15;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Task03 {
    public static void main(String[] args) {
        noZ();
        noYY();
        no9();
        noLong();
        two2();
        noTeen();
        no34();
        square56();
    }

    private static void fillingInteger(List<Integer> number) {
        for (int i = 1; i <= 20; i++) {
            number.add(i);
        }
    }

    private static void fillingString(List<String> string) {
        string.add("f");
        string.add("fg");
        string.add("gdf");
        string.add("dgzy");
        string.add("sdtez");
        string.add("Dwevyy");
        string.add("vwevyyx");
        string.add("vwevyyfz");
        string.add("Dwevxyxfw");
        string.add("Dwevxyyfwf");
    }

    private static void noZ() {
        // 11
        List<String> noZ = new ArrayList<>();
        fillingString(noZ);
        noZ = noZ.stream().filter(a -> !a.contains("z")).collect(Collectors.toList());
        System.out.println(noZ);
    }

    private static void noYY() {
        //12
        List<String> noYY = new ArrayList<>();
        fillingString(noYY);
        noYY = noYY.stream().map(a -> a + "y").filter(a -> !a.contains("yy")).collect(Collectors.toList());
        System.out.println(noYY);
    }

    private static void no9() {
        //13
        List<Integer> no9 = new ArrayList<>();
        no9.add(429);
        no9.add(42);
        fillingInteger(no9);
        no9.add(139);
        no9 = no9.stream().filter(a -> 9 != a % 10).collect(Collectors.toList());
        System.out.println(no9);
    }

    private static void noLong() {
        // 14
        List<String> noLong = new ArrayList<>();
        fillingString(noLong);
        noLong = noLong.stream().filter(a -> a.length() < 4).collect(Collectors.toList());
        System.out.println(noLong);
    }

    private static void two2() {
        //15
        List<Integer> two2 = new ArrayList<>();
        fillingInteger(two2);
        two2 = two2.stream().map(a -> a * 2).filter(a -> 2 != a % 10).collect(Collectors.toList());
        System.out.println(two2);
    }

    private static void noTeen() {
        // 16
        List<Integer> noTeen = new ArrayList<>();
        fillingInteger(noTeen);
        System.out.println(noTeen + "   3");
        noTeen = noTeen.stream().filter(a -> a < 13 || a > 19).collect(Collectors.toList());
        System.out.println(noTeen);
    }

    private static void no34() {
        // 17
        List<String> no34 = new ArrayList<>();
        fillingString(no34);
        no34 = no34.stream().filter(a -> a.length() < 3 || a.length() > 4).collect(Collectors.toList());
        System.out.println(no34);
    }

    private static void square56() {
        // 18
        List<Integer> square56 = new ArrayList<>();
        fillingInteger(square56);
        square56 = square56.stream().map(a -> a * a + 10).filter(a -> 5 != a % 10 && 6 != a % 10).collect(Collectors.toList());
        System.out.println(square56);
    }
}