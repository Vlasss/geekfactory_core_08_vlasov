package ru.vlasov.s15;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Task01 {
    public static void main(String[] args) {
        doubling();
        copies3();
        rightDigit();
        square();
        moreY();
    }

    private static void fillingInteger(List<Integer> number) {
        for (int i = 1; i <= 20; i++) {
            number.add(i);
        }
    }

    private static void fillingString(List<String> string) {
        string.add("f");
        string.add("fg");
        string.add("gdf");
        string.add("dgzy");
        string.add("sdtez");
        string.add("Dwevyy");
        string.add("vwevyyx");
        string.add("vwevyyfz");
        string.add("Dwevxyxfw");
        string.add("Dwevxyyfwf");
    }

    private static void doubling() {
        //  1
        List<Integer> doubling = new ArrayList<>();
        fillingInteger(doubling);
        doubling = doubling.stream().map(a -> a * 2).collect(Collectors.toList());
        System.out.println(doubling);
    }

    private static void copies3() {
        // 2
        List<String> copies = new ArrayList<>();
        fillingString(copies);
        copies = copies.stream().map(a -> a + a + a).collect(Collectors.toList());
        System.out.println(copies);
    }

    private static void rightDigit() {
        // 3
        List<Integer> rightDigit = new ArrayList<>();
        fillingInteger(rightDigit);
        rightDigit = rightDigit.stream().map(a -> a % 10).collect(Collectors.toList());
        System.out.println(rightDigit);
    }

    private static void square() {
        //  4
        List<Integer> square = new ArrayList<>();
        fillingInteger(square);
        square = square.stream().map(a -> a * a).collect(Collectors.toList());
        System.out.println(square);
    }

    private static void moreY() {
        //  5
        List<String> moreY = new ArrayList<>();
        fillingString(moreY);
        moreY = moreY.stream().map(a -> a + "Y").collect(Collectors.toList());
        System.out.println(moreY);
    }
}