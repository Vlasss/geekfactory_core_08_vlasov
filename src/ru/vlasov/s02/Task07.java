package ru.vlasov.s02;

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите 3-х значное число");
        int number = scanner.nextInt();
        searchPalindrom(number);
    }

    private static void searchPalindrom(Integer number) {
        String numberString = number.toString();
        if (numberString.charAt(0)==numberString.charAt(2)) {
            System.out.println("Палиндром");
        } else System.out.println("Не палиндром");
    }
}