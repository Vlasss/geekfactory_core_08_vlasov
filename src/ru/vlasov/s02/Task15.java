package ru.vlasov.s02;

import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите колличество * в последней строке ");
        int charactersLastLine = sc.nextInt();
        buildPyramid(charactersLastLine);
    }

    public static void buildPyramid(int charactersLastLine) {
        if ((charactersLastLine % 2) == 0) {
            charactersLastLine++;
        }

        for (int i = 0; i < charactersLastLine; i++) {

            for (int j = 0; j < charactersLastLine - i; j++) {
                System.out.print(" ");
            }

            for (int k = 0; k <= i; k++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}