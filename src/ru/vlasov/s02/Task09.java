package src.ru.vlasov.s02;

import java.util.Scanner;

public class Task09 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите x ");
        double x = sc.nextDouble();
        count(x);
    }

    private static void count(double x) {
        if (x <= 0) {
            System.out.println("y = " + 0);
        } else if (x > 0 & x < 1) {
            System.out.println("y = " + x);
        } else
            System.out.println("y = " + Math.pow(x, 2));
    }
}