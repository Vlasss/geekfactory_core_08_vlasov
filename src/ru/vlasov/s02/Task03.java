package ru.vlasov.s02;

import java.util.Scanner;

public class Task03 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число");
        int number1 = scanner.nextInt();
        System.out.println("Введите второе число");
        int number2 = scanner.nextInt();
        Task03.define(number1, number2);
    }

    private static void define(int number1, int number2) {
        if (number1 > number2) {
            System.out.println(" Первое число больше ");
        } else {
            System.out.println(" Второе число больше ");
        }
    }
}