package ru.vlasov.s02;

import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите колличество * в последней строке ");
        int charactersLastLine = sc.nextInt();
        buildTriangle(charactersLastLine);
    }

    public static void buildTriangle(int charactersLastLine) {
        for (int i = 0; i < charactersLastLine; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}