package ru.vlasov.s02;

import java.util.Scanner;

import static java.lang.Math.sin;

public class Task02 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите x ");
        int x = scanner.nextInt();
        count(x);

    }

    public static void count(int x) {
        double result;
        if (x > 0) {
            result = sin(Math.toRadians(x)) * sin(Math.toRadians(x));
            System.out.println("y = " + result);
        } else {
            result = 1 - 2 * (sin(Math.toRadians(Math.pow(x,2))));
            System.out.println("y = " + result);
        }
    }
}