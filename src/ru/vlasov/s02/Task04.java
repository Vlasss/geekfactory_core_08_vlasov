package ru.vlasov.s02;

import java.util.Scanner;

public class Task04 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите дистанцию в километрах");
        int distanceKm = scanner.nextInt();
        System.out.println("Введите дистанцию в футах");
        int distanceFt = scanner.nextInt();
        count(distanceKm, distanceFt);
    }

    public static void count(int distanceKm, int distanceFt) {
        double fut = 0.305;
        if (distanceKm > (distanceFt * fut)) {
            System.out.println("Дистанция в " + distanceKm + "километров будет быстрее");
        } else {
            System.out.println("Дистанция в " + distanceFt + "футов будет быстрее");
        }
    }
}