package ru.vlasov.s02;


import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        int number = Task08.enter();
        Task08.comparison(number);
    }

    public static int enter() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите трехзначное число :");
        int number = sc.nextInt();
        return number;
    }

    public static void comparison(int number) {
        int number1 = number / 100;
        int number2 = (number / 10) % 10;
        int number3 = number % 10;
        if ((number1 == number2) || (number1 == number3) || (number2 == number3)) {
            System.out.println("Среди цифр есть одинаковые");
        } else {
            System.out.println("Среди цифр нет одинаковых");
        }
    }
}