package ru.vlasov.s02;

import java.util.Scanner;

public class Task10 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Выберите способ : ");
        String method = sc.nextLine();
        System.out.println("Введите день недели: ");
        int DayOfTheWeek = sc.nextInt();
        if (method.equals("case")) {
            getDayOfWeek(DayOfTheWeek);
        } else if (method.equals("array")) {
            weekarray(DayOfTheWeek);
        } else {
            System.out.println("Параметр задан неверно");
        }
    }

    public static void weekarray(int DayOfTheWeek) {
        String[] week = {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"};
        System.out.println(week[DayOfTheWeek - 1]);

    }

    public static void getDayOfWeek(int DayOfTheWeek) {
        switch (DayOfTheWeek) {
            case 1:
                System.out.println("Понедельник");
                break;
            case 2:
                System.out.println("Вторник");
                break;
            case 3:
                System.out.println("Среда");
                break;
            case 4:
                System.out.println("Четверг");
                break;
            case 5:
                System.out.println("Пятница");
                break;
            case 6:
                System.out.println("Суббота");
                break;
            case 7:
                System.out.println("Воскресенье");
                break;
            default:
                System.out.println("N/A");
        }
    }
}