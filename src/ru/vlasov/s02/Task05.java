package ru.vlasov.s02;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Double radiusCircle = enterCircle();
        Double sideSquare = enterSquare();
        areaComparison(countAreaSquare(sideSquare), countAreaCircle(radiusCircle));
    }

    private static Double enterSquare() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите сторону квадрата :");
        return sc.nextDouble();
    }

    private static Double enterCircle() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите радиус круга :");
        return sc.nextDouble();
    }

    private static void areaComparison(Double areaSquare, Double areaCircle) {
        if (areaCircle > areaSquare) {
            System.out.println(areaCircle);
            System.out.println("Площадь круга больше");
        } else {
            System.out.println(areaSquare);
            System.out.println("Площадь квадрата больше");
        }
    }

    private static Double countAreaCircle(Double radiusCircle) {
        return Math.pow(radiusCircle, 2) * 3.14;
    }

    private static Double countAreaSquare(Double sideSquare) {
        return Math.pow(sideSquare, 2);
    }
}