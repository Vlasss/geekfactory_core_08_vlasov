package ru.vlasov.s12;

public class SelectionSort {
    public static int[] methodSelectionSort(int[] array) {
        int buf;
        int minElementFoArray;
        for (int min = 0; min < array.length; min++) {
            minElementFoArray = min;
            for (int j = min + 1; j < array.length; j++) {
                if (array[j] < array[minElementFoArray]) {
                    minElementFoArray = j;
                }
            }
            buf = array[min];
            array[min] = array[minElementFoArray];
            array[minElementFoArray] = buf;
        }
        return array;
    }
}