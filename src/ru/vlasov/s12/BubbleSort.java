package ru.vlasov.s12;

public class BubbleSort {
    public static int[] bubbleSort(int[] array) {
        int temporaryVariable;
        for (int m = array.length - 1; m >= 1; m--)
            for (int i = 0; i < m; i++) {
                if (array[i] > array[i + 1]) {
                    temporaryVariable = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temporaryVariable;
                }
            }
        return array;
    }
}