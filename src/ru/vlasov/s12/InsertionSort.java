package ru.vlasov.s12;

public class InsertionSort {
    public static int[] insertionSort(int[] array) {
        int key;
        int buf;
        for (int i = 0; i < array.length - 1; i++) {
            key = i + 1;
            buf = array[key];
            for (int j = i + 1; j > 0; j--) {
                if (buf < array[j - 1]) {
                    array[j] = array[j - 1];
                    key = j - 1;
                }
            }
            array[key] = buf;
        }
        return array;
    }
}